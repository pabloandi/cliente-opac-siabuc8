# -*- coding: utf-8 -*-
import sys, ConfigParser as cfgparser, os, MySQLdb as mysql, pyodbc as pg, re, types, datetime
from decimal import Decimal
from time import time

config=cfgparser.RawConfigParser()
config.read('config.ini')

def convertir(x):
	if type(x) == types.NoneType or str(x) == "":
		return "NULL"
	elif type(x) == types.StringType or type(x) == type(datetime.datetime(2010,10,10)) or type(x) == type(Decimal("1.000")):
		return "'%s'" % (re.escape(unicode(str(x).strip(),config.get('hostopac','codificacion')).replace('\n','').replace('\r','')))
		#return "'%s'" % (re.escape(str(x).decode(config.get('hostopac','codificacion')).strip().replace('\n','').replace('\r','')))
	elif type(x) == types.IntType or type(x) == types.LongType:
		return int(x)
	elif type(x) == types.FloatType:
		return float(x)
	elif type(x) == types.BooleanType:
		return x
	else:
		return x	

def escapar(dic):
	k=dic.keys()
	v=dic.values()
	v=map(convertir,v)
	return dict(zip(k,v))

def logear(texto):
	try:
		log=open('log.txt','a')
		print texto
		log.write("[%s : acervo] %s \n" % (datetime.datetime.now(),texto))
		log.close()
	except IOError:
	   print "No se pudo abrir el archivo "
	   sys.exit(1)	

inicio=time()
print "******** iniciando proceso de acervo ***********"

if os.system("ping %s -n 2" % config.get('hostsiabuc','host')) or os.system("ping %s -n 2" % config.get('hostopac','host')):
	print "Error en la conexion hacia internet"
	sys.exit (1)
else:
	
	try:
		siabuc=pg.connect("DRIVER={Microsoft Access Driver (*.mdb)} ;DBQ=%s" % config.get('hostsiabuc','dsn'), ansi=True).cursor()
	except pg.InternalError, e:
		logear("ERROR en la conexion con siabuc -> %s : %s" % (e.args[0], e.args[1]))
		sys.exit (1)

	try:
		db=mysql.connect(host=config.get('hostopac','host'), user=config.get('hostopac','user'),passwd=config.get('hostopac','password'),db=config.get('hostopac','database'))
		db.set_character_set('utf8')
		opac=db.cursor()
	except mysql.Error, e:
		logear("Error en la conexion con el opac -> %s: %s" % (e.args[0], e.args[1]))
		sys.exit (1)
	
	##truncar las tablas
	
	tablas=['EjemplaresGeneral','FichasGeneral','UsuariosGeneral']
	##for tabla in tablas:
	##	try:
	##		opac.execute("TRUNCATE TABLE %s " % tabla)
	##		print "truncando tabla %s " % tabla
	##	except opac.Error, e:
	##		logear("ERROR no se pudo truncar la tabla %s -> %s : %s"  % (tabla, e.args[0], e.args[1]))
	##		sys.exit (1)
	
	##ejemplares general
	print "******* ingresando datos en la tabla Ejemplares *******"
	try:
		for TD in siabuc.execute("""
			select 
			('L' & ej.Ficha_No) AS Ficha_No,
			ej.NumAdqui,
			%(biblioteca)s AS Biblioteca,
			NULL AS Ano,
			ej.Volumen,
			NULL AS Numero,
			ej.Ejemplar
			from 
			((EJEMPLARES AS ej)
			INNER JOIN Bibliotecas AS b ON b.No=ej.Biblioteca)
			
			UNION ALL

			select 
			('R' & e.Ficha_No) AS Ficha_No,
			e.NumAdqui AS NumAdqui,
			%(biblioteca)s AS Biblioteca,
			e.`A�o` AS Ano,
			e.Volumen AS Volumen,
			e.Numero AS Numero,
			e.NumCopia AS Ejemplar
			from 
			`Existencias Revistas` as e
			INNER JOIN Bibliotecas ON Bibliotecas.No=e.Biblioteca
			
		""" % {'biblioteca':config.get('hostsiabuc','biblioteca')}).fetchall():
			TD=map(convertir,TD)
			sql="REPLACE INTO EjemplaresGeneral (Ficha_no,NumAdqui,Biblioteca,Ano,Volumen,Numero,Ejemplar) VALUES (%s,%s,%s,%s,%s,%s,%s)" % tuple(TD)
			
			opac.execute(sql)
	except pg.Error, e:
		logear("ERROR no se pudo ejecutar la seleccion de Ejemplares en siabuc -> %s " % e)
		sys.exit (1)
	except opac.Error, e:
		logear("ERROR no se pudo ejecutar el ingreso de datos de Ejemplares -> %s : %s" % (e.args[0], e.args[1]))
		sys.exit (1)
	except UnicodeError, e:
		print "error de codificacion -> %s : %s" % (e.args[0], e.args[1])
	except KeyboardInterrupt, e:
		print sql
		print TD
		for a in TD:
			print type(a)
	
	##fichas general
	print "******* ingresando datos en la tabla Fichas *******"
	try:
		for TD in siabuc.execute("""
			SELECT
				'L' & f.Ficha_No AS Ficha_No,
				f.EtiquetasMARC,
				f.Titulo,
				f.Autor,
				f.Clasificacion,
				f.ISBN,
				null AS FechaPublicacion,
				tm.Descripcion AS TipoMaterial,
				%(biblioteca)s AS Biblioteca
			FROM
				Fichas as  f LEFT JOIN `Tipos de Material` as tm ON tm.IDTipoMaterial=f.TipoMaterial
			
			UNION ALL

			SELECT
				('R' & fr.Ficha_No) AS Ficha_No, 
				fr.EtiquetasMARC,
				fr.Titulo,
				null AS Autor,
				fr.Clasificacion, 
				fr.ISSN AS ISBN,
				null AS FechaPublicacion,
				'Revista' AS TipoMaterial,
				%(biblioteca)s AS Biblioteca
			FROM 
				`Fichas Revistas` as fr
			
			UNION ALL

			SELECT
				('A' & a.ID) AS Ficha_No, 
				(('�000' & a.TituloRevista) & a.EtiquetasMARC) AS EtiquetasMARC, 
				IIF( 
					inStr (1 , a.EtiquetasMARC, '245' )=0 OR  inStr (1 , a.EtiquetasMARC, '245' )=NULL, 
					NULL,  
					mid (a.EtiquetasMARC , inStr (1 , a.EtiquetasMARC, '245' ) + 3,  
					IIF( 
						(inStr ( inStr (1 , a.EtiquetasMARC, '245' ) , a.EtiquetasMARC, '�'  )  ) = 0,
						(inStr ( inStr (1 , a.EtiquetasMARC, '245' ) , a.EtiquetasMARC, '�'  )  )  -  inStr (1 , a.EtiquetasMARC, '245' ) - 3,
						(inStr ( inStr (1 , a.EtiquetasMARC, '245' ) , a.EtiquetasMARC, '�'  )  )  -  inStr (1 , a.EtiquetasMARC, '245' ) - 3 )))  AS Titulo,
				IIF(  
					inStr (1 , a.EtiquetasMARC, '100' )=0 OR inStr (1 , a.EtiquetasMARC, '100' )=NULL, 
					NULL, 
					mid (a.EtiquetasMARC , inStr (1 , a.EtiquetasMARC, '100' ) + 3,  
					IIF( 
						(inStr ( inStr (1 , a.EtiquetasMARC, '100' ) , a.EtiquetasMARC, '�'  )  ) = 0,
						(inStr ( inStr (1 , a.EtiquetasMARC, '100' ) , a.EtiquetasMARC, '�'  )  )  -  inStr (1 , a.EtiquetasMARC, '100' ) - 3,
						(inStr ( inStr (1 , a.EtiquetasMARC, '100' ) , a.EtiquetasMARC, '�'  )  )  -  inStr (1 , a.EtiquetasMARC, '100' ) - 3 ))) AS Autor, 
				null AS Clasificacion, 
				null AS ISBN,
				null AS FechaPublicacion,
				'Art�culo de Revista' AS TipoMaterial,
				%(biblioteca)s AS Biblioteca

			FROM 
			Analiticas as a
			
			""" % {'biblioteca':config.get('hostsiabuc','biblioteca')}).fetchall():
			TD=map(convertir,TD)
			sql="REPLACE INTO FichasGeneral VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)" % tuple(TD)
			opac.execute(sql)
	except pg.ProgrammingError, e:
		logear("ERROR no se pudo ejecutar la seleccion de Fichas en siabuc -> %s " % e)
		print sql
		sys.exit (1)
	except opac.Error, e:
		logear("ERROR no se pudo ejecutar el ingreso de datos de Fichas -> %s : %s" % (e.args[0], e.args[1]))
		sys.exit (1)
	except UnicodeError, e:
		print "error de codificacion -> %s : %s" % (e.args[0], e.args[1])
	except Exception , exp:
		print TD
		print exp
		print type(exp)
		sys.exit (1)
	
	##usuarios general
	print "******* ingresando datos en la tabla Usuarios *******"
	try:
		for TD in siabuc.execute("""
			select 
			u.NoCuenta,
			u.Nombre, 
			g.`Descripci�n` AS NoGrupo, 
			e.`Nombre` AS NoEscuela, 
			u.`E-mail` AS Email,
			u.Domicilio, 
			u.Colonia, 
			u.`Ciudad Estado` AS CiudadEstado, 
			u.Telefono,
			u.Notas, 
			(SELECT SUM(Monto) FROM MultasPendientes WHERE MultasPendientes.NoCuenta=u.NoCuenta ) AS Multa
			from
			(((Usuarios AS u)
			INNER JOIN Grupos AS g ON g.No=u.NoGrupo)
			INNER JOIN Escuelas AS e ON e.No=u.NoEscuela)
			""").fetchall():
			TD=map(convertir,TD)
			sql="REPLACE INTO UsuariosGeneral VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)" % tuple(TD)
			opac.execute(sql)
	except pg.ProgrammingError, e:
		logear("ERROR no se pudo ejecutar la seleccion de Usuarios en siabuc -> %s " % e)
		sys.exit (1)
	except opac.Error, e:
		logear("ERROR no se pudo ejecutar el ingreso de datos de Usuarios -> %s : %s" % (e.args[0], e.args[1]))
		sys.exit (1)
	except UnicodeError, e:
		print "error de codificacion -> %s : %s" % (e.args[0], e.args[1])
	
	
	
	fin=time()
	logear("Proceso de acervo realizado en %s segundos " % (fin-inicio))

