# -*- coding: utf-8 -*-
import sys, ConfigParser as cfgparser, os, MySQLdb as mysql, pyodbc as pg, re, types, datetime
from decimal import Decimal
from time import time

config=cfgparser.RawConfigParser()
config.read('config.ini')

def convertir(x):
	if type(x) == types.NoneType or str(x) == "":
		return "NULL"
	elif type(x) == types.StringType or type(x) == type(datetime.datetime(2010,10,10)) or type(x) == type(Decimal("1")):
		#return "'%s'" % (re.escape(unicode(str(x).strip(),config.get('hostopac','codificacion')).replace('\n','').replace('\r','')))
		return "'%s'" % (re.escape(str(x).decode(config.get('hostopac','codificacion')).strip().replace('\n','').replace('\r','')))
	elif type(x) == types.IntType or type(x) == types.LongType:
		return int(x)
	elif type(x) == types.FloatType:
		return float(x)
	elif type(x) == types.BooleanType:
		return x
	else:
		return x	

def escapar(dic):
	k=dic.keys()
	v=dic.values()
	v=map(convertir,v)
	return dict(zip(k,v))

def logear(texto):
	try:
		log=open('log.txt','a')
		print texto
		log.write("[%s : prestamos] %s \n" % (datetime.datetime.now(),texto))
		log.close()
	except IOError:
	   print "No se pudo abrir el archivo "
	   sys.exit(1)

inicio=time()
print "******** iniciando proceso de prestamos ***********"
	
if os.system("ping %s -n 2" % config.get('hostsiabuc','host')) or os.system("ping %s -n 2" % config.get('hostopac','host')):
	logear("ERROR conexion hacia internet")
	sys.exit (1)
else:
		
	try:
		siabuc=pg.connect("DRIVER={Microsoft Access Driver (*.mdb)} ;DBQ=%s" % config.get('hostsiabuc','dsn'), ansi=True).cursor()
	except pg.InternalError, e:
		logear("ERROR en la conexion con siabuc -> %s " % e)
		sys.exit (1)

	try:
		db=mysql.connect(host=config.get('hostopac','host'), user=config.get('hostopac','user'),passwd=config.get('hostopac','password'),db=config.get('hostopac','database'))
		db.set_character_set('utf8')
		opac=db.cursor()
	except mysql.Error, e:
		logear("Error en la conexion con el opac -> %s: %s" % (e.args[0], e.args[1]))
		sys.exit (1)
	
	##truncar las tablas
	
	tablas=['PrestamosGeneral','Reservados','MultasPendientes']
	##for tabla in tablas:
	##	try:
	##		opac.execute("TRUNCATE TABLE %s " % tabla)
	##		print "truncando tabla %s " % tabla
	##	except opac.Error, e:
	##		logear("ERROR no se pudo truncar la tabla %s -> %s : %s"  % (tabla, e.args[0], e.args[1]))
	##		sys.exit (1)
	
	##prestamos general
	print "******* ingresando datos en la tabla Prestamos *******"
	try:
		for TD in siabuc.execute("""
			select 
				NoCuenta, 
				NumAdqui, 
				Nombre, 
				Titulo, 
				Autor, 
				Clasificacion, 
				`Fecha Salida` AS FechaSalida, 
				`Fecha Entrega` AS FechaEntrega, 
				'Prestamo' AS TipoPrestamo,
				%(biblioteca)s AS Biblioteca
			from
				Prestamos
			UNION
			select
				NoCuenta, 
				NumAdqui, 
				Nombre, 
				Titulo, 
				Autor, 
				Clasificacion, 
				NULL AS FechaSalida, 
				NULL AS FechaEntrega, 
				'Prestamo interno' AS TipoPrestamo,
				%(biblioteca)s AS Biblioteca
			from
				`Prestamos Internos`
			""" % {'biblioteca':config.get('hostsiabuc','biblioteca')}).fetchall():
			TD=map(convertir,TD)
			sql="REPLACE INTO PrestamosGeneral VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)" % tuple(TD)
			opac.execute(sql)
	except pg.ProgrammingError, e:
		logear("ERROR no se pudo ejecutar la seleccion de prestamos en siabuc -> %s " % e)
		sys.exit (1)
	except opac.Error, e:
		logear("ERROR no se pudo ejecutar el ingreso de datos de prestamos -> %s : %s" % (e.args[0], e.args[1]))
		sys.exit (1)
	except UnicodeError, e:
		print "error de codificacion -> %s : %s" % (e.args[0], e.args[1])
	
	##reservaciones	
	print "******* ingresando datos en la tabla Reservaciones *******"
	try:
		for TD in siabuc.execute("""
		SELECT 
			numadqui,
			fechainicial, 
			fechafinal,
			NoCuenta, 
			%(biblioteca)s AS Biblioteca
		FROM 
			Reservados""" % {'biblioteca':config.get('hostsiabuc','biblioteca')}).fetchall():
			TD=map(convertir,TD)
			sql="REPLACE INTO Reservados VALUES (%s,%s,%s,%s,%s)" % tuple(TD)
			opac.execute(sql)
	except pg.ProgrammingError, e:
		logear("ERROR no se pudo ejecutar la seleccion de grupos en siabuc -> %s" % e)
		sys.exit (1)
	except opac.Error, e:
		logear("ERROR no se pudo ejecutar el ingreso de datos de grupos -> %s : %s" % (e.args[0], e.args[1]))
		sys.exit (1)
	except UnicodeError, e:
		print "error de codificacion -> %s : %s" % (e.args[0], e.args[1])
	
	##multas_pendientes	
	print "******* ingresando datos en la tabla Multas pendientes *******"
	try:
		for TD in siabuc.execute("""
		SELECT 
			NoCuenta, 
			FechaDevolucion, 
			Monto, 
			Observaciones, 
			idCapturista, 
			idBloqueo 
		FROM 
			MultasPendientes
		""").fetchall():
			TD=map(convertir,TD)
			sql="REPLACE INTO MultasPendientes (NoCuenta, FechaDevolucion, Monto, Observaciones, idCapturista, idBloqueo) VALUES (%s,%s,%s,%s,%s,%s)" % tuple(TD)
			opac.execute(sql)
	except pg.ProgrammingError, e:
		logear("ERROR no se pudo ejecutar la seleccion de multas pendientes en siabuc -> %s " % e)
		sys.exit (1)
	except opac.Error, e:
		logear("ERROR no se pudo ejecutar el ingreso de datos de multas pendientes -> %s : %s" % (e.args[0], e.args[1]))
		sys.exit (1)
	except UnicodeError, e:
		print "error de codificacion -> %s : %s" % (e.args[0], e.args[1])
	
	fin=time()
	logear("Proceso de prestamos realizado en %s segundos " % (fin-inicio))